from tulip.symbol import sym, revsym
from tulip.byte import Instr
import tulip.value as v
from tulip.value import dump_value
from tulip.builtin import invoke_builtin

from tulip.debug import debug
debug_runtime = debug.check('runtime')

t_sym = sym(u't')
f_sym = sym(u'f')

class Runtime(object):
    def __init__(self):
        self.procs = []
        self.pid_counter = 0

    def spawn(self, callable, arguments):
        self.pid_counter += 1
        proc = Proc(self, self.pid_counter)
        self.procs.append(proc)
        callable.invoke(proc, arguments)

    def spawn_root(self, bytecode):
        self.pid_counter += 1
        proc = Proc(self, self.pid_counter)
        self.procs.append(proc)
        proc.push_frame(bytecode, [], [])
        return proc

    def run(self):
        if debug_runtime: print 'running %d procs' % len(self.procs)
        proc_index = 0

        while True:
            if len(self.procs) == 0:
                return

            if proc_index >= len(self.procs):
                proc_index = 0

            proc = self.procs[proc_index]

            if proc._finished:
                del self.procs[proc_index]
            else:
                proc.step()
                proc_index += 1

class Proc(object):
    def __init__(self, rt, pid):
        self.rt = rt
        self.pid = pid

        self.call_stack = []
        self._finished = False
        self._finished_reason = None

        self.result = None

    def invoke_builtin(self, name, arity, arguments):
        return invoke_builtin(name, arity, self, arguments)

    def push_frame(self, bytecode, closure, arguments):
        self.call_stack.append(Frame(self, bytecode, closure, arguments))

    def push_value(self, value):
        # the last frame may have been collected due to being in tail
        # position. in this case, we know there is nothing that will
        # use the given value, so we can safely do nothing.
        if not self.call_stack:
            return

        self.call_stack[-1].stack.append(value)

    def finish(self, reason):
        self._finished = True
        self._finished_reason = reason

    def dump_result(self):
        out = ''
        if self._finished:
            out += self._finished_reason
            if self.result:
                out += ': ' + dump_value(self.result)
        else:
            out += 'running... (%s stack frames)' % len(self.call_stack)

        return out

    def returned(self):
        return self._finished and self._finished_reason == 'returned'

    def crash_reason(self):
        return self._finished_reason

    def step(self):
        if self._finished:
            return False

        if self.call_stack:
            self.call_stack[-1].step()
            return self._finished
        else:
            self._finished = True
            self._finished_reason = 'returned'
            return False

    def crash(self, message):
        if debug_runtime: print 'crashing', message
        self._finished = True
        self._finished_reason = "crashed: %s" % message

    def return_frame(self):
        if len(self.call_stack) == 1:
            if self.call_stack[0].stack:
                self.result = self.call_stack[0].stack[-1]
            self.finish("returned")
        else:
            frame = self.call_stack.pop()
            self.call_stack[-1].stack.append(frame.stack[-1])

class Frame(object):
    def __init__(self, proc, bytecode, closure, arguments):
        self.proc = proc
        self.bytecode = bytecode
        self.closure = closure
        self.arguments = arguments
        self.locals = [None] * bytecode.locals_count()
        self.stack = []
        self.leftover_arguments = None

        # load up the instructions
        self.code = bytecode.instrs
        self.code_pointer = 0

    def get_arg(self, idx):
        return self.arguments[idx]

    def get_local(self, idx):
        return self.locals[idx]

    def set_local(self, idx, value):
        assert self.locals[idx] is None, "double assign to %d" % idx
        self.locals[idx] = value

    def get_closure(self, idx):
        return self.closure[idx]

    def get_const(self, idx):
        return self.bytecode.constants[idx]

    def revsym(self, idx):
        return self.bytecode.symbols[idx]

    def get_lambda(self, idx):
        return self.bytecode.lambdas[idx]

    # implements tulip apply/curry semantics
    def apply(self, function, arguments):
        if debug_runtime: print 'apply(%s, %s)' % (dump_value(function), ', '.join(map(dump_value, arguments)))
        assert len(arguments) > 0

        if not function.IS_CALLABLE:
            self.proc.crash("tried to call uncallable: %s" % function.dump())
            return

        arg_count = len(arguments)
        arity = function.arity()

        if arity == 0:
            self.crash("tried to call arity 0: %s" % function.dump())
            return

        if arg_count == arity and not isinstance(function, v.Builtin) and self.is_tail_position():
            if debug_runtime: print 'tail position', self.proc.call_stack
            # tail call - pop this frame off since
            # all it has left to do is return.
            self.proc.call_stack.pop()

        if arg_count >= arity:
            # we are saturated
            applied_args = arguments[0:arity]

            function.invoke(self.proc, applied_args)

            if arg_count > arity:
                if debug_runtime: print 'setting leftover_arguments', ' '.join([a.dump() for a in arguments[arity:]])
                self.leftover_arguments = arguments[arity:]
        else:
            self.stack.append(function.curry(arguments))

    def is_tail_position(self):
        if self.code_pointer < len(self.code) and \
                self.code[self.code_pointer][0] != Instr.RETRN: return False
        if self.leftover_arguments is not None: return False

        # important so that we never collect the root frame
        # of a proc.
        if len(self.proc.call_stack) <= 1: return False

        return True

    def step(self):
        if self.leftover_arguments:
            leftover_arguments, self.leftover_arguments = self.leftover_arguments, None
            self.apply(self.stack.pop(), leftover_arguments)
            return

        if self.code_pointer >= len(self.code):
            # implicit return
            self.proc.return_frame()
            return

        (instr, arg1, arg2) = self.code[self.code_pointer]
        self.code_pointer += 1

        if instr == Instr.LOADA:
            self.stack.append(self.get_arg(arg1))
        elif instr == Instr.LOADC:
            self.stack.append(self.get_closure(arg1))
        elif instr == Instr.LOADL:
            self.stack.append(self.get_local(arg1))
        elif instr == Instr.CONST:
            self.stack.append(self.get_const(arg1))
        elif instr == Instr.STORL:
            value = self.stack.pop()
            self.set_local(arg1, value)
        elif instr == Instr.LAMBD:
            lam = v.Func(self.get_lambda(arg1), self.popn(arg2))
            self.stack.append(lam)
        elif instr == Instr.BLTIN:
            name = self.revsym(arg1)
            arity = arg2
            self.stack.append(v.Builtin(name, arity))
        elif instr == Instr.MKTAG:
            args = self.popn(arg1)
            symbol = self.revsym(arg2)
            self.stack.append(v.Tagged(symbol, args))
        elif instr == Instr.DETAG:
            index = arg1
            value = self.stack.pop()
            if isinstance(value, v.Tagged) and len(value.args) > index:
                self.stack.append(value.args[index])
            else:
                self.proc.crash('cannot detag %s at %d', value.dump(), index)

        elif instr == Instr.APPLY:
            function = self.stack.pop()
            arguments = self.popn(arg1)
            self.apply(function, arguments)
        elif instr == Instr.TESTB:
            value = self.stack.pop()
            if value.matches_tag('t', 0):
                self.code_pointer += 1
            elif not value.matches_tag('f', 0):
                self.proc.crash("invalid boolean: %s" % value.dump())
        elif instr == Instr.TESTT:
            symbol = self.revsym(arg1)
            value = self.stack.pop()
            if value.matches_tag(symbol, arg2):
                self.code_pointer += 1
        elif instr == Instr.TESTN:
            symbol = self.revsym(arg1)
            if value.matches_native(symbol):
                self.code_pointer += 1
        elif instr == Instr.JUMP:
            new_index = arg1
            self.code_pointer = new_index
            if new_index >= len(self.code):
                self.proc.crash("unmatched case")

        elif instr == Instr.BANG:
            self.stack.append(v.bang)

        elif instr == Instr.PATCH:
            closure_index = arg1
            closed = self.stack.pop()
            value = self.stack.pop()
            if closed.HAS_CLOSURE:
                closed.patch_closure(closure_index, value)
            else:
                self.proc.crash('cannot patch %s', closed.dump())

        elif instr == Instr.POP:
            pop_size = arg1
            self.popn(pop_size)

        elif instr == Instr.RETRN:
            self.proc.return_frame()

    def popn(self, n):
        assert len(self.stack) >= n, 'empty stack - tried to pop %d, only %d remaining' % (n, len(self.stack))

        stack_index = len(self.stack) - n
        out = self.stack[stack_index:]
        del self.stack[stack_index:]
        return out
