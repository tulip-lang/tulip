### [jneen] XXX HACK TODO: actually make a prelude ###
import tulip.semantic as s
from tulip.symbol import sym

sym_counter = [0]
def builtin_sym():
    sym_counter[0] += 1
    return sym('##builtin##%d' % sym_counter[0])

def builtin_let(builtin):
    vars = [builtin_sym() for _ in xrange(0, builtin['arity'])]
    names = [s.Name(v) for v in vars]
    builtin_call = s.Builtin(builtin['name'], builtin['arity'], names)

    return s.Let(sym(builtin['name']), s.nested_lambda(vars, builtin_call))

def with_prelude(expr):
    # TODO
    return expr

### [jneen] END HACK ###
