from tulip.symbol import sym
from tulip.compiler.util import split_lines, try_split, is_tok, get_tok
from tulip.lexer import Token
from tulip.compiler.pattern import LetPatternBuilder
from tulip.compiler.lam import split_section, compile_clauses

import tulip.semantic as s

class BlockElement(object):
    def compile(self, context):
        assert False, 'abstract'

class SimpleLet(BlockElement):
    def __init__(self, name, expr):
        self.name = name
        self.expr = expr

    def compile(self, context):
        symbol = sym(get_tok(self.name).value)
        return s.Let(symbol, context.compile_expr(self.expr))

class LambdaLet(BlockElement):
    def __init__(self, name, clause):
        self._name = name
        self.clause = clause

    def name(self):
        return get_tok(self._name).value

# irrefutable pattern, like
# { .foo x = thing }
class IrrefutableLet(BlockElement):
    def __init__(self, pattern, body):
        self.pattern = pattern
        self.body = body

    def compile(self, context):
        context.error('TODO: irrefutable patterns')
        # temp = context.gensym(u'l')
        # parts.append(s.Let(temp, bound_expr))

        # builder = LetPatternBuilder(context)
        # builder.compile_seq(before, temp)
        # parts.extend(builder.directives)

class LambdaGroup(BlockElement):
    def __init__(self, name, elements):
        self.name = name
        self.elements = elements

    def compile(self, context):
        clauses = [split_section(let.clause, Token.EQ) for let in self.elements]
        lam = compile_clauses(clauses, context)
        return s.Let(sym(self.name), lam)

class BlockExpr(BlockElement):
    def __init__(self, expr):
        self.expr = expr

    def compile(self, context):
        return context.compile_expr(self.expr)

def compile_block(expr, context):
    elements = parse_block_elements(expr, context)

    parts = []
    last_lambda_name = None
    last_lambda_group = []

    for element in parse_block_elements(expr, context):
        if isinstance(element, LambdaLet):
            if last_lambda_group and not element.name() == last_lambda_name:
                # flush
                parts.append(LambdaGroup(last_lambda_name, last_lambda_group))
                last_lambda_name = None

            last_lambda_group.append(element)
            last_lambda_name = element.name()
        else:
            if last_lambda_group:
                parts.append(LambdaGroup(last_lambda_name, last_lambda_group))
                last_lambda_name = None

            parts.append(element)

    if last_lambda_group:
        parts.append(LambdaGroup(last_lambda_name, last_lambda_group))

    compiled = [p.compile(context) for p in parts]

    if len(compiled) == 1 and not isinstance(compiled[0], s.Let):
        return compiled[0]
    else:
        return s.Block(compiled)

def parse_block_elements(expr, context):
    elements = []
    lines = split_lines(expr)

    for (i, line) in enumerate(lines):
        assert len(line) > 0, u'empty line!'

        (equal_sign, before, after) = try_split(line, Token.EQ)

        if equal_sign:
            if not before:
                context.error('let with nothing before the `=` sign')
                continue

            if is_tok(before[0], Token.NAME):
                if len(before) == 1:
                    yield SimpleLet(before[0], after)
                else:
                    yield LambdaLet(before[0], line[1:])
            else:
                yield IrrefutableLet(before, after)
        else:
            yield BlockExpr(line)

def compile_let_run(let_run, context):
    # TODO: recursion
    assert False, u'TODO: compile let runs'

