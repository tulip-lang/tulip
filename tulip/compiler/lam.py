from .util import split_at, get_tok, seq_contains, list_contains, is_tok, underscore_sym
import tulip.value as v
import tulip.semantic as s
from tulip.lexer import Token
from tulip.symbol import sym

class Dispatch(object):
    pass

class Body(Dispatch):
    def __init__(self, body):
        self.body = body

    def compile(self, context):
        return context.compile_expr(self.body)

class Guard(Dispatch):
    def __init__(self, clauses):
        self.clauses = clauses

    def compile(self, context):
        out = [None] * len(self.clauses)
        for (i, (cond, body)) in enumerate(self.clauses):
            directive = s.MatchGuard(context.compile_expr(cond))
            result = context.compile_expr(body)
            out[i] = s.MatchClause([directive], result)

        return s.FlatteningMatch(out)

def detect_arity(pattern):
    if is_tok(pattern[0], Token.TAGGED):
        return 1
    else:
        return len(pattern)

def check_arities(clauses, context):
    arity = -1
    for s in clauses:
        this_arity = detect_arity(s[0])
        if arity < 0:
            arity = this_arity
        elif arity != this_arity:
            context.error(get_tok(clauses[0][0][0]), u'unmatched pattern arities')
            return -1

    if arity < 0:
        return 1
    else:
        return arity

def split_clause(clause_toks, splitter):
    return split_at(clause_toks, splitter, max=2, err=u'empty clause')

def split_section(clause_toks, splitter):
    if list_contains(clause_toks, Token.QUESTION):
        parts = split_at(clause_toks, Token.QUESTION)
        pat = parts[0]
        clauses = parts[1:]
        return (pat, Guard([split_clause(c, splitter) for c in clauses]))
    else:
        pat, clause = split_clause(clause_toks, splitter)
        return (pat, Body(clause))

def compile_lambda(body, context):
    lines = split_at(body, Token.NL)
    clauses = [split_section(line, Token.RARROW) for line in lines]
    return compile_clauses(clauses, context)

def compile_clauses(clauses, context):
    arity = check_arities(clauses, context)
    if arity <= 0: return

    binders = [context.gensym(u'arg') for _ in xrange(0, arity)]

    match_clauses = [None] * len(clauses)

    for (i, (pattern, dispatch)) in enumerate(clauses):
        directives = context.compile_pattern(pattern, binders)

        match_clauses[i] = s.MatchClause(directives, dispatch.compile(context))

    return s.Lambda(binders, match_clauses)

def compile_autolam(expr, context):
    assert False, u'TODO'

