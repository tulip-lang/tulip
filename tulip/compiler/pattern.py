from tulip.compiler.util import get_tok, seq_contains, split_at, nested_sym, get_initial_tok, get_body
from tulip.value import rpy_list
from tulip.lexer import Token
from tulip.symbol import sym
import tulip.value as v
import tulip.semantic as s

class PatternBuilder(object):
    def __init__(self, context):
        self.directives = []
        self.context = context

    def guard(self, value):
        self.directives.append(s.MatchGuard(value))

    def guard_tag(self, value, tag, arity):
        self.directives.append(s.MatchGuardTag(value, sym(tag), arity))

    def assign(self, symbol, value):
        self.directives.append(s.MatchAssign(symbol, value))

    def compile_seq(self, pattern, arg):
        first_tok = get_tok(pattern[0])

        if first_tok.tokid == Token.TAGGED:
            self.guard_tag(s.Name(arg), first_tok.value, len(pattern) - 1)

            for (i, sub_pat) in enumerate(pattern[1:]):
                destructed = destruct_match(s.Name(arg), i)

                tok = get_tok(sub_pat)
                if tok and tok.tokid == Token.NAME:
                    self.assign(sym(tok.value), destructed)
                else:
                    sub_arg = self.context.gensym(u'd')
                    self.assign(sub_arg, destructed)
                    self.compile_term(sub_pat, sub_arg)

        elif len(pattern) > 1:
            self.context.error(get_tok(pattern[0]), u'more than one expr is not allowed here')
        else:
            return self.compile_term(pattern[0], arg)


    def compile_term(self, term, arg):
        print 'compile_term', term.dump()

        tok = get_tok(term)
        if tok:
            if tok.tokid == Token.NAME:
                self.assign(sym(tok.value), s.Name(arg))
            elif tok.tokid == Token.TAGGED:
                self.guard_tag(s.Name(arg), tok.value, 0)

            return

        sub_pat = get_body(term, Token.LPAREN)
        if sub_pat:
            self.compile_seq(sub_pat, arg)
            return

        self.context.error(get_initial_tok(term), u'unexpected token in pattern')

class LetPatternBuilder(PatternBuilder):
    def guard(self, value):
        self.directives.append(s.CrashGuard(value))

    def guard_tag(self, value, tag, arity):
        self.directives.append(s.CrashGuardTag(value, sym(tag), arity))

    def assign(self, symbol, value):
        self.directives.append(s.Let(symbol, value))


def test_match(arg, tag, arity):
    return s.TagMatch(sym(tag), 3, s.Name(arg))

def destruct_match(e, i):
    return s.TagDestruct(i, e)

def compile_pattern(pattern, binders, context):
    assert len(pattern) > 0

    # mutable
    builder = PatternBuilder(context)

    first_tok = get_tok(pattern[0])

    if first_tok.tokid == Token.TAGGED:
        # should be guaranteed by check_arities
        assert len(binders) == 1
        builder.compile_seq(pattern, binders[0])
    else:
        for (i, sub_pat) in enumerate(pattern):
            builder.compile_term(sub_pat, binders[i])

    return builder.directives

