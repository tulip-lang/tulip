class Instr(object):
    INSTRS = """
    LOADL
    LOADA
    LOADC

    CONST

    STORL

    LAMBD
    MKTAG
    DETAG

    JUMP

    TESTB
    TESTT
    TESTN

    RETRN

    BLTIN

    APPLY

    BANG
    PATCH

    POP
    """.split()

for (i, instr) in enumerate(Instr.INSTRS):
    setattr(Instr, instr, i)

class Code(object):
    def locals_count(self):
        assert False, 'abstract'

    def dump(self):
        assert False, 'abstract'

    def dump_instr(self, instr):
        (t, a1, a2) = instr
        return "    %s %s %s" % (Instr.INSTRS[t], self.dump_digit(a1), self.dump_digit(a2))

    def dump_digit(self, digit):
        if digit < 0:
            return '-'
        else:
            return str(digit)

    def dump_instrs(self):
        out = ''
        for (i, instr) in enumerate(self.instrs):
            out += '%d:%s\n' % (i, self.dump_instr(instr))

        return out


class RootCode(Code):
    def __init__(self, instrs, local_names, symbols, constants, lambdas, trace_name):
        self.instrs = instrs
        self.local_names = local_names
        self.local_count = len(local_names)
        self.symbols = symbols
        self.constants = constants
        self.lambdas = lambdas
        self.trace_name = trace_name

    def brief_dump(self):
        return '(root)'

    def dump(self):
        out = ''
        out += "[constants]\n"
        for (i, const) in enumerate(self.constants):
            out += "#%d: %s\n" % (i, const.dump())
        out += "[symbols]\n"
        for (i, sym) in enumerate(self.symbols):
            out += "#%d: %s\n" % (i, sym)
        out += "[root l:%s]\n" % self.local_count
        out += self.dump_instrs()
        out += "[lambdas]\n"
        for (i, l) in enumerate(self.lambdas):
            out += l.dump()

        return out

    def locals_count(self):
        return self.local_count

class Lambda(Code):
    def __init__(self, instrs, index, arg_count, closed_count, local_count):
        self.instrs = instrs
        self.index = index
        self.arg_count = arg_count
        self.closed_count = closed_count
        self.local_count = local_count

    def locals_count(self):
        return self.local_count

    def brief_dump(self):
        return "lambda#%d(a:%d, c:%d, l:%d)" % (self.index, self.arg_count, self.closed_count, self.local_count)

    def dump(self):
        out = ''
        out += self.brief_dump() + ':\n'
        out += self.dump_instrs()

        return out
