from rpython.rlib.rarithmetic import r_uint
from tulip.symbol import sym
from tulip.value import rpy_list, cons_list, cons_sym, cons_each, malformed

from tulip.compiler.symbols import *

from tulip.debug import debug
debug_compiler = debug.check('compiler')

from .lam import compile_lambda
from .expr import compile_expr
from .pattern import compile_pattern
from .block import compile_block
from .prelude import with_prelude
from .module import ModuleCompiler

class CompileError(StandardError):
    def __init__(self, errors):
        self.errors = errors

    def dump(self):
        messages = [_make_message(t, m) for (t, m) in self.errors]
        return u'\n'.join(messages)

def _make_message(token, message):
    return u'compile error at %s: %s' % (token.dump(), message)

class Compiler(object):
    def __init__(self):
        self.macros = {}
        self.modules = {}
        self.errors = []
        self.gensym_counter = r_uint(0)

    def gensym(self, base):
        self.gensym_counter += 1
        return sym(u'#%s%d#' % (base, self.gensym_counter))

    def compile_module(self, skeleton):
        mod = ModuleCompiler(self)
        mod.compile(skeleton)
        self.modules[mod.get_name()] = mod.get_module()

    def error(self, tok, message):
        self.errors.append((tok, message))

    def check_errors(self):
        if len(self.errors) > 0:
            raise CompileError(self.errors)

class CompileContext(object):
    def __init__(self, compiler, module):
        self.compiler = compiler
        self.module = module

    def gensym(self, base):
        return self.compiler.gensym(base)

    def error(self, tok, message):
        self.compiler.error(tok, message)

    def compile_expr(self, e):
        return compile_expr(e, self)

    def compile_lambda(self, e):
        return compile_lambda(e, self)

    def compile_pattern(self, pattern, binders):
        return compile_pattern(pattern, binders, self)

    def compile_block(self, b):
        return compile_block(b, self)

def compile_root_expr(expr):
    if expr.matches_tag('nil', 0):
        return None

    context = CompileContext(Compiler(), None)
    out = compile_block(expr, context)
    if debug_compiler: print 'compiled: ', out.dump()
    context.compiler.check_errors()
    return out

def compile_module(expr):
    if expr.matches_tag('nil', 0):
        return None

    compiler = Compiler()
    out = compiler.compile_module(module)
    compiler.check_errors()
    return out
