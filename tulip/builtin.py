import tulip.value as v

builtins = {}
def invoke_builtin(name, arity, proc, arguments):
    return builtins[name].invoke(arity, proc, arguments)

def lookup_builtin(name):
    return builtins[name]

class Builtin(object):
    def __init__(self, name, impl, arity):
        self.impl = impl
        self.arity = arity

    def invoke(self, arity, proc, arguments):
        if not arity == self.arity:
            return proc.crash('builtin called with %d arguments, expected %d')

        return self.impl(proc, arguments)

def declare_builtin(name, arity):
    def decorate(fn):
        builtins[name] = Builtin(name, fn, arity)
        return fn
    return decorate

@declare_builtin('prelude.add', 2)
def add(proc, arguments):
    first = arguments[0]
    second = arguments[1]

    if not isinstance(first, v.Int) and isinstance(second, v.Int):
        return proc.crash('invalid arguments to add')

    return v.Int(first.value + second.value)

@declare_builtin('prelude.print', 1)
def print_builtin(proc, arguments):
    arg = arguments[0]
    if isinstance(arg, v.String):
        print arg.value
    else:
        print arg.dump()

    return v.tag('ok', [])
