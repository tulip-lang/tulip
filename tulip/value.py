from tulip.symbol import sym
import tulip.lexer as L

class MalformedValue(ValueError):
    pass

def malformed(message):
    raise MalformedValue(message)

class Value(object):
    IS_CALLABLE = False
    IS_BUILTIN = False
    IS_NAMESPACE = False
    HAS_CLOSURE = False

    def dump(self):
        return repr(self)

    def dump_nested(self):
        return self.dump()

    def matches_tag(self, tag, arity):
        return False

    def matches_type(self, name):
        return False

    def is_list(self):
        return self.matches_tag('nil', 0) or self.matches_tag('cons', 2)

    def patch_closure(self, index, value):
        assert False, 'not a closure'

def dump_value(v):
    if isinstance(v, Value):
        return v.dump()
    else:
        return repr(v)

int_sym = sym(u'int')
class Int(Value):
    def __init__(self, value):
        self.value = value

    def matches_type(self, name):
        return name == int_sym

    def dump(self):
        return unicode(str(self.value))

string_sym = sym(u'str')
class String(Value):
    def __init__(self, value):
        self.value = value

    def matches_type(self, name):
        return name == string_sym

    def dump(self):
        return u"'{%s}" % self.value

class Bang(Value):
    def __init__(self):
        pass

    def dump(self):
        return u'!'

bang = Bang()

class Token(Value):
    def __init__(self, value):
        self.value = value

    def dump(self):
        tok_name = L.Token.TOKENS[self.value.tokid]
        if self.value.value is None:
            return u'(*Token %s)' % tok_name
        else:
            return u'(*Token %s %s)' % (tok_name, self.value.value)

class Callable(Value):
    IS_CALLABLE = True

    def arity(self):
        assert False, "abstract"

    def invoke(self, proc, arguments):
        assert False, "abstract"

    def curry(self, arguments):
        return Curry(self, arguments)

class Func(Callable):
    HAS_CLOSURE = True

    def __init__(self, bytecode, closure):
        self.recursive = [False] * len(closure)

        self.bytecode = bytecode
        for c in closure:
            assert c, 'uh, none?'
        self.closure = closure

    def dump(self):
        if self.closure:
            return '<func %s : %s>' % (self.bytecode.brief_dump(), self.dump_closure())
        else:
            return '<func %s>' % (self.bytecode.brief_dump())

    def dump_closure(self):
        out = []
        for i in xrange(0, len(self.closure)):
            if self.recursive[i]:
                out.append(u'<...>')
            else:
                out.append(self.closure[i].dump())
        return u' '.join(out)


    def arity(self):
        return self.bytecode.arg_count

    def invoke(self, proc, arguments):
        proc.push_frame(self.bytecode, self.closure, arguments)

    def patch_closure(self, index, value):
        self.recursive[index] = True
        self.closure[index] = value

class Builtin(Callable):
    def __init__(self, name, impl):
        self.name = name
        self.impl = impl

    def dump(self):
        return '#%s/%d' % (self.name, self.impl.arity)

    def arity(self):
        return self.impl.arity

    def invoke(self, proc, arguments):
        value = self.impl.invoke(len(arguments), proc, arguments)
        proc.push_value(value)

class Curry(Callable):
    def __init__(self, callable, curry):
        self.callable = callable
        self.curry = curry

    def dump(self):
        curry_ = ' '.join([v.dump() for v in self.curry])
        return '<curry %s %s>' % (self.callable.dump(), curry_)

    def arity(self):
        return self.callable.arity() - len(self.curry)

    def curry(self, arguments):
        return Curry(self.callable, self.curry + arguments)

    def invoke(self, proc, arguments):
        self.callable.invoke(proc, self.curry + arguments)

class Module(Callable):
    def __init__(self, namespace, tree, arity):
        self.namespace = namespace
        self.tree = tree
        self.arity = arity

class Bang(Value):
    def dump(self):
        return '(!)'

bang = Bang()

cons_sym = sym(u'cons')
nil_sym = sym(u'nil')

def cons_list(elements):
    i = len(elements)
    out = tag(u'nil', [])
    while i > 0:
        i -= 1
        out = tag(u'cons', [elements[i], out])

    return out

def rpy_list(value):
    out = []
    for e in cons_each(value):
        out.append(e)
    return out

def cons_each(value):
    while True:
        if value.matches_tag('cons', 2):
            yield value.args[0]
            value = value.args[1]
        elif value.matches_tag('nil', 0):
            return
        else:
            raise MalformedValue(u'rpy_list: bad cons-list')

class Tagged(Value):
    def __init__(self, name, args):
        self.name = name
        self.args = args

    def matches_tag(self, tag, arity):
        return self.name == tag and len(self.args) == arity

    def dump(self):
        if self.is_list():
            return self.inspect_cons_list()

        if len(self.args) == 0:
            return u'.%s' % self.name
        else:
            args = u' '.join([a.dump_nested() for a in self.args])
            return u'.%s %s' % (self.name, args)

    def inspect_cons_list(self):
        els = [e.dump() for e in rpy_list(self)]

        return u'/[%s]' % u'; '.join(els)

    def dump_nested(self):
        if self.is_list():
            return self.inspect_cons_list()

        return u'(%s)' % self.dump()

def tag(name, values):
    return Tagged(name, values)

TRUE = tag(u't', [])
FALSE = tag(u'f', [])

# TODO
class Pattern(Value):
    def __init__(self, bytecode):
        self.bytecode = bytecode
