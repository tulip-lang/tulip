from tulip.libedit import readline
from tulip.reader import StringReader
from tulip.lexer import ReaderLexer, Token, LexError
from tulip.skeleton import parse_skeleton, UnmatchedError, ParseError
from tulip.compiler import compile_root_expr, CompileError
from tulip.scope_tracker import ScopeTracker, ScopeError
from tulip.module_resolver import ModuleProvider, PreludeProvider
from tulip.byte_compiler import ByteCompiler
from tulip.byte import RootCode
from tulip.runtime import Runtime
from tulip.value import dump_value, rpy_list, cons_list

from tulip.compiler.util import try_split, is_tok, get_tok

import tulip.semantic as s

repl_char_map = {
    Token.LPAREN: u'(',
    Token.LBRACE: u'{',
    Token.LBRACK: u'[',
    Token.MACRO: u'['
}

class Repl(object):
    def __init__(self):
        self.lines = []
        self.last_unmatched = None
        self.definitions = {}

    def prompt(self):
        if self.last_unmatched:
            return '%s: ' % str(repl_char_map[self.last_unmatched.tokid])
        else:
            return ' : '

    def lexer(self):
        return ReaderLexer(StringReader(u'<repl>', u'\n'.join(self.lines)))

    def process_line(self):
        try:
            code = u'\n'.join(self.lines)
            skeleton = parse_skeleton(self.lexer())

            ast = compile_root_expr(skeleton)

            if ast is None:
                print u'(no input)'
                return

            tracker = ScopeTracker(ReplProvider(self), PreludeProvider())
            ast = tracker.resolve(ast)

            compiler = ByteCompiler()
            bytecode = compiler.compile(ast)

            runtime = Runtime()
            proc = runtime.spawn_root(bytecode)
            runtime.run()

            if not proc.returned():
                print ' !', proc.crash_reason()
                return

            frame = proc.call_stack[0]

            assert isinstance(frame.bytecode, RootCode)
            for i in xrange(0, frame.bytecode.locals_count()):
                name = frame.bytecode.local_names[i].name

                # don't set internal variables
                if name[0] == '#':
                    continue

                value = frame.get_local(i)
                self.definitions[name] = value
                print name, '=', value.dump()

            if proc.result:
                print 'it =', dump_value(proc.result)
                self.definitions[u'it'] = proc.result

            self.last_unmatched = None
        except UnmatchedError as e:
            self.last_unmatched = e.token
        except LexError as e:
            print e.dump()
        except ParseError as e:
            print e.dump()
        except CompileError as e:
            print e.dump()
        except ScopeError as e:
            print e
        finally:
            if self.last_unmatched is None:
                self.lines = []

    def run(self):
        while True:
            try:
                line = readline(self.prompt())
                self.lines.append(line)
                self.process_line()
            except EOFError:
                return 0
            except KeyboardInterrupt:
                return 1

class ReplProvider(ModuleProvider):
    def __init__(self, repl):
        self.repl = repl

    def lookup(self, symbol):
        return s.Constant(self.repl.definitions[symbol.name])

