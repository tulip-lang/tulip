import tulip.semantic as s
import tulip.value as v
from tulip.symbol import sym

class ModuleProvider(object):
    def lookup(self, symbol):
        assert False, 'abstract'

add_sym_id = sym('add').id
print_sym_id = sym('print').id

class PreludeProvider(object):
    def lookup(self, symbol):
        if symbol.id == add_sym_id:
            return s.Builtin(sym(u'prelude.add'))
        elif symbol.id == print_sym_id:
            return s.Builtin(sym(u'prelude.print'))
        else:
            raise KeyError

