from tulip.symbol import Symbol

class Semantic(object):
    def dump(self):
        assert False, 'abstract'

    # handle dispatch for the bytecode visitor
    def visit(self, visitor):
        assert False, 'abstract'

    def visit_children(self, visitor):
        return self


class Constant(Semantic):
    def __init__(self, value):
        self.value = value

    def dump(self):
        return u'<const %s>' % self.value.dump()

    def visit(self, visitor):
        return visitor.visit_constant(self)

class Apply(Semantic):
    def __init__(self, nodes):
        self.nodes = nodes

    def dump(self):
        return u'<apply [%s]>' % u' '.join([e.dump() for e in self.nodes])

    def visit(self, visitor):
        return visitor.visit_apply(self)

    def visit_children(self, visitor):
        return Apply([visitor.visit(n) for n in self.nodes])

def make_apply(segments):
    if len(segments) == 1:
        return segments[0]
    else:
        return Apply(segments)

class Closeable(Semantic):
    pass

class Closed(Semantic):
    def contains_forwards(self):
        return self.max_forward_index() >= 0

    def max_forward_index(self):
        index = -1
        for n in self.closed:
            if isinstance(n, ForwardName) and n.index > index:
                index = n.index

        return index

    def reindex(self, index):
        for n in self.closed:
            if isinstance(n, ForwardName):
                n.index -= index

        return self

class Lambda(Closeable):
    def __init__(self, binders, clauses):
        assert isinstance(binders, list)
        self.binders = binders
        self.clauses = clauses

    def dump(self):
        binders_ = u' '.join([b.name for b in self.binders])
        clauses_ = u' '.join([c.dump() for c in self.clauses])
        return u'<lambda %s %s>' % (binders_, clauses_)

    def visit(self, visitor):
        return visitor.visit_lambda(self)

    def visit_children(self, visitor):
        return Lambda(self.binders, [visitor.visit(c) for c in self.clauses])

class ClosedLambda(Closed):
    def __init__(self, closed, locals, binders, clauses):
        assert isinstance(closed, list)
        self.closed = closed
        self.binders = binders
        self.locals = locals
        self.clauses = clauses

    def visit(self, visitor):
        return visitor.visit_closed_lambda(self)

    def visit_children(self, visitor):
        closed = [visitor.visit(c) for c in self.closed]
        clauses = [visitor.visit(c) for c in self.clauses]
        return ClosedLambda(closed, self.locals, self.binders, clauses)

    def dump(self):
        binders_ = u' '.join([b.name for b in self.binders])
        closed_ = u' '.join([c.dump() for c in self.closed])
        clauses_ = u' '.join([c.dump() for c in self.clauses])
        return u'<closedlambda (%s) %s %s>' % (closed_, binders_, clauses_)

def nested_lambda(binders, body):
    return Lambda(binders, body)

class Block(Semantic):
    def __init__(self, nodes):
        self.nodes = nodes

    def dump(self):
        return u'<block [%s]>' % u'; '.join([e.dump() for e in self.nodes])

    def visit(self, visitor):
        return visitor.visit_block(self)

    def visit_children(self, visitor):
        return Block([visitor.visit(n) for n in self.nodes])

class Match(Semantic):
    def __init__(self, clauses):
        for c in clauses:
            assert isinstance(c, MatchClause)

        self.clauses = clauses

    def dump(self):
        clauses = u' '.join([c.dump() for c in self.clauses])
        return u'<match %s>' % clauses

    def visit(self, visitor):
        return visitor.visit_branch(self)

    def visit_children(self, visitor):
        return Match([visitor.visit(c) for c in self.clauses])

class FlatteningMatch(Match):
    pass

class MatchClause(Semantic):
    def __init__(self, directives, result):
        self.directives = directives
        self.result = result

    def dump(self):
        directives_ = u' '.join([d.dump() for d in self.directives])
        return u'<clause [%s] = %s>' % (directives_, self.result.dump())

    def visit(self, visitor):
        return visitor.visit_match_clause(self)

    def visit_children(self, visitor):
        return MatchClause([visitor.visit(d) for d in self.directives], visitor.visit(self.result))

class MatchDirective(Semantic):
    pass

class MatchAssign(MatchDirective):
    def __init__(self, symbol, value, index=-1):
        assert isinstance(value, Semantic)
        self.symbol = symbol
        self.value = value
        self.index = index

    def dump(self):
        if self.index >= 0:
            return u'<assign %s/%d %s>' % (self.symbol.name, self.index, self.value.dump())
        else:
            return u'<assign %s %s>' % (self.symbol.name, self.value.dump())

    def visit(self, visitor):
        return visitor.visit_match_assign(self)

    def visit_children(self, visitor):
        return MatchAssign(self.symbol, visitor.visit(self.value))

class MatchGuard(MatchDirective):
    def __init__(self, value):
        self.value = value

    def visit(self, visitor):
        return visitor.visit_match_guard(self)

    def visit_children(self, visitor):
        return MatchGuard(visitor.visit(self.value))

    def dump(self):
        return u'<guard %s>' % (self.value.dump())

class MatchGuardTag(MatchDirective):
    def __init__(self, value, tag, arity):
        self.value = value
        self.tag = tag
        self.arity = arity

    def visit(self, visitor):
        return visitor.visit_match_guard_tag(self)

    def visit_children(self, visitor):
        return MatchGuardTag(visitor.visit(self.value), self.tag, self.arity)

    def dump(self):
        return u'<guard-tag %s %s/%d>' % (self.value.dump(), self.tag.name, self.arity)

class Name(Semantic):
    def __init__(self, symbol):
        self.symbol = symbol

    def dump(self):
        return u'<name %s>' % self.symbol.name

    def visit(self, visitor):
        return visitor.visit_name(self)

class ScopedName(Name):
    def __init__(self, symbol, index):
        self.symbol = symbol
        self.index = index

    def dump(self):
        return u'<%s %s/%d>' % (self.SCOPE_TYPE, self.symbol.name, self.index)

class LocalName(ScopedName):
    SCOPE_TYPE = u'local'
    def visit(self, visitor):
        return visitor.visit_local_name(self)

class ArgumentName(ScopedName):
    SCOPE_TYPE = u'arg'
    def visit(self, visitor):
        return visitor.visit_argument_name(self)

class ClosureName(ScopedName):
    SCOPE_TYPE = u'closure'
    def visit(self, visitor):
        return visitor.visit_closure_name(self)

class ForwardName(ScopedName):
    SCOPE_TYPE = u'forward'
    def visit(self, visitor):
        return visitor.visit_forward_name(self)

class ScopedLet(Semantic):
    def __init__(self, bind, index, body):
        self.bind = bind
        self.index = index
        self.body = body

    def visit(self, visitor):
        return visitor.visit_scoped_let(self)

    def visit_children(self, visitor):
        return ScopedLet(self.bind, self.index, visitor.visit(self.body))

    def dump(self):
        return u'<scoped-let %s/%d %s>' % (self.bind.name, self.index, self.body.dump())

class ScopedLetRec(Semantic):
    def __init__(self, names, values):
        self.names = names
        self.values = values

    def visit(self, visitor):
        return visitor.visit_scoped_letrec(self)

    def visit_children(self, visitor):
        return ScopedLetRec(self.names, [visitor.visit(v) for v in self.values])

    def dump(self):
        names_ = ['%s/%d' % (n.name, i) for (n, i) in self.names]
        assigns_ = ['%s = %s' % (n, v.dump()) for (n, v) in zip(names_, self.values)]

        return u'<scoped-letrec %s>' % ' | '.join(assigns_)

class ModuleName(Name):
    def __init__(self, namespace, symbol):
        self.namespace = namespace
        self.symbol = symbol

    def dump(self):
        namespace_ = u'/'.join([n.name for n in self.namespace])
        return u'<modname %s/%s>' % (namespace_, self.symbol.name)

    def visit(self, visitor):
        return visitor.visit_module_name(self)

class Tag(Semantic):
    def __init__(self, symbol, values):
        assert isinstance(symbol, Symbol)

        self.symbol = symbol
        self.values = values

    def dump(self):
        if len(self.values) > 0:
            values_ = u' '.join([v.dump() for v in self.values])
            return u'<tag .%s [%s]>' % (self.symbol.name, values_)
        else:
            return u'<tag .%s>' % self.symbol.name

    def visit(self, visitor):
        return visitor.visit_tag(self)

    def visit_children(self, visitor):
        return Tag(self.symbol, [visitor.visit(v) for v in self.values])

class TagMatch(Semantic):
    def __init__(self, tag, arity, value):
        assert isinstance(tag, Symbol)
        self.tag = tag
        self.arity = arity
        self.value = value

    def dump(self):
        return u'<tag-match %s/%d %s>' % (self.tag.name, self.arity, self.value.dump())

    def visit(self, visitor):
        return visitor.visit_tag_match(self)

    def visit_children(self, visitor):
        return TagMatch(self.tag, self.arity, visitor.visit(self.value))

class TagDestruct(Semantic):
    def __init__(self, index, value):
        self.index = index
        self.value = value

    def dump(self):
        return u'<tag-destruct #%d %s>' % (self.index, self.value.dump())

    def visit(self, visitor):
        return visitor.visit_tag_destruct(self)

    def visit_children(self, visitor):
        return TagDestruct(self.index, visitor.visit(self.value))

class Flag(Semantic):
    def __init__(self, symbol):
        self.symbol = symbol

    def dump(self):
        return u'<flag -%s>' % self.symbol.name

    def visit(self, visitor):
        return visitor.visit_flag(self)

class FlagMap(Semantic):
    def __init__(self, pairs):
        self.pairs = pairs

    def dump(self):
        pairs_ = u' '.join([u"-%s: %s" % (s.name, e.dump()) for (s, e) in self.pairs])
        return u'<flag-map %s>' % pairs_

    def visit(self, visitor):
        return visitor.visit_flag_map(self)

    def visit_children(self, visitor):
        out = [] * len(self.pairs)
        for (i, (symbol, semantic)) in enumerate(self.pairs):
            out[i] = (symbol, visitor.visit(semantic))

        return FlagMap(out)

class Let(Semantic):
    def __init__(self, bind, body):
        self.bind = bind
        self.body = body

    def dump(self):
        return u'<let %s %s>' % (self.bind.dump(), self.body.dump())

    def visit(self, visitor):
        return visitor.visit_let(self)

    def visit_children(self, visitor):
        return Let(self.bind, visitor.visit(self.body))

def let(sym, val, rest):
    if isinstance(rest, Block):
        return Block([Let(sym, val)] + rest.nodes)
    else:
        return Block([Let(sym, val), rest])

class Builtin(Semantic):
    def __init__(self, name):
        self.name = name

    def dump(self):
        return u'<builtin %s>' % self.name.name

    def visit(self, visitor):
        return visitor.visit_builtin(self)

class Visitor(object):
    def __init__(self):
        assert False, 'abstract'

    def visit_default(self, semantic):
        return semantic.visit_children(self)

    visit_constant         = visit_default
    visit_apply            = visit_default
    visit_lambda           = visit_default
    visit_closed_lambda    = visit_default
    visit_block            = visit_default
    visit_branch           = visit_default
    visit_match_clause     = visit_default
    visit_match_assign     = visit_default
    visit_match_guard      = visit_default
    visit_match_guard_tag  = visit_default
    visit_match_guard_type = visit_default
    visit_name             = visit_default
    visit_local_name       = visit_default
    visit_argument_name    = visit_default
    visit_closure_name     = visit_default
    visit_module_name      = visit_default
    visit_forward_name     = visit_default
    visit_tag              = visit_default
    visit_tag_match        = visit_default
    visit_tag_destruct     = visit_default
    visit_flag             = visit_default
    visit_flag_map         = visit_default
    visit_let              = visit_default
    visit_scoped_let       = visit_default
    visit_scoped_letrec    = visit_default
    visit_builtin          = visit_default

    def visit(self, semantic):
        out = semantic.visit(self)
        if out is None:
            return semantic
        else:
            return out
