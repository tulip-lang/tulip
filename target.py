from tulip.symbol import Symbol, SymbolTable
from sys import stdin
from tulip.debug import debug
from rpython.rlib.objectmodel import we_are_translated
from tulip.lexer import ReaderLexer, Token, LexError
from tulip.reader import StringReader, FileReader
from tulip.skeleton import parse_skeleton, UnmatchedError, ParseError
from tulip.compiler import compile_expr, compile_module, CompileError
from tulip.scope_tracker import ScopeTracker
from tulip.byte_compiler import ByteCompiler
from tulip.module_resolver import PreludeProvider
from tulip.repl import Repl
import tulip.runtime as r

import traceback

import tulip.tests.runtime as tests

def entry_point(argv):
    if len(argv) >= 2:
        return run_file(argv[1])

    if stdin.isatty:
        return run_repl()

    assert False, u'TODO: actually implement an arg parser'

repl = Repl()
def run_repl():
    print_logo()

    repl.run()

def print_logo():
    print
    print u"    )`("
    print u"   (   ) tulip"
    print u"     |/"
    print

def run_file(fname):
    reader = FileReader(fname)
    try:
        skeleton = parse_skeleton(ReaderLexer(reader)).dump()
        namespace = compile_module(skeleton)
        print namespace.dump()
        namespace.compile_all()

    except LexError as e:
        print e.dump()
    except ParseError as e:
        print e.dump()

    return 0

def target(*args):
    return (entry_point, None)

if __name__ == '__main__':
    from sys import argv
    entry_point(argv)
