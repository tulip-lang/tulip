from .symbol import sym, revsym
import tulip.semantic as s

class UnboundName(StandardError):
    pass

class LinkContext(object):
    def lookup(self, name):
        assert False, 'abstract'

    def missing(self):
        raise UnboundName

class Linker(s.Visitor):
    def __init__(self, context):
        assert isinstance(context, LinkContext)

        self.context = context
        self.unbound_names = []

    def link(semantic):
        out = self.visit(semantic)
        if self.unbound_names:
            self.link_error()

        return out

    def visit_module_name(self, name):
        try:
            return context.lookup(name.symbol)
        except UnboundName:
            self.unbound_names.append(name)
            return name
