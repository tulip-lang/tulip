from tulip.symbol import sym

chain_sym = sym(u'#chain#')
autovar_sym = sym(u'$')
underscore_sym = sym(u'_')
t_sym = sym(u't')
f_sym = sym(u'f')
nil_sym = sym(u'nil')
