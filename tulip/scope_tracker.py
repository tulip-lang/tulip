from .symbol import sym, revsym
import tulip.semantic as s

from tulip.debug import debug
debug_scope = debug.check('compiler') or debug.check('scope')

class BindingTracker(object):
    def __init__(self, args, parent):
        self.args = args
        self.parent = parent

        self.local_index = 0
        self.forwards = []
        self.locals = []
        self.all_locals = []
        self.block_indices = []

        self.closure = []

    def start_block(self):
        self.block_indices.append(len(self.locals))

    def bind(self, symbol):
        self.locals.append((symbol, self.local_index))
        self.all_locals.append(symbol)
        self.local_index += 1
        return self.local_index - 1

    def bind_forward(self, symbol, letrec_index):
        self.forwards.append((symbol, letrec_index))

    def clear_forwards(self):
        self.forwards = []

    def end_block(self):
        assert len(self.block_indices) > 0
        index = self.block_indices.pop()
        assert index <= len(self.locals)
        del self.locals[index:]

    def has_own_bound_local(self, var):
        i = len(self.locals)
        while i > 0:
            i -= 1
            (symbol, index) = self.locals[i]
            if var.id == symbol.id:
                return True

        return False

    def lookup(self, var):
        i = len(self.locals)
        while i > 0:
            i -= 1
            (symbol, index) = self.locals[i]
            if var.id == symbol.id:
                return s.LocalName(var, index)

        i = len(self.forwards)
        while i > 0:
            i -= 1
            (symbol, index) = self.forwards[i]
            if var.id == symbol.id:
                ### XXX HACK XXX ###
                # only works because we later bind them in
                # sequence index order...
                local_index = self.local_index + index
                return s.ForwardName(var, local_index)

        i = len(self.args)
        while i > 0:
            i -= 1
            if self.args[i].id == var.id:
                return s.ArgumentName(var, i)

        if self.parent is None:
            raise KeyError

        # may raise out if unbound
        closed = self.parent.lookup(var)

        self.closure.append(closed)
        return s.ClosureName(var, len(self.closure) - 1)

class ScopeError(StandardError):
    pass

class LetGroup(object):
    def __init__(self, names, values):
        self.size = len(names)
        self.names = names
        self.values = values

    def compile(self, tracker):
        assert False, 'abstract'

class LetRec(LetGroup):
    def compile(self, tracker):
        resolved = [None] * self.size
        local_indices = [None] * self.size

        for (i, name) in enumerate(self.names):
            tracker.binding_tracker.bind_forward(name, i)

        for (i, name) in enumerate(self.names):
            resolved[i] = tracker.visit(self.values[i])

            # bind normally, since after this point it's no longer
            # a forward-decl. this is also where we make sure a local
            # index is available for the declaration.
            local_indices[i] = tracker.binding_tracker.bind(name)

        # grab all the regular lets that don't use forwards from the beginning
        i = 0
        out = []
        while i < self.size:
            while i < self.size:
                assert isinstance(resolved[i], s.Closed), 'forward-decls must be closed'
                if resolved[i].contains_forwards(): break
                out.append(s.ScopedLet(self.names[i], local_indices[i], resolved[i]))

                i += 1

            forward_names = []
            forward_values = []
            max_forward_index = -1
            while i < self.size:
                assert isinstance(resolved[i], s.Closed), 'forward-decls must be closed'
                forward_index = resolved[i].max_forward_index()


                if forward_index > max_forward_index:
                    max_forward_index = forward_index

                # we're done if we've included the last forwarded assignment
                if i > max_forward_index: break

                forward_names.append((self.names[i], local_indices[i]))
                forward_values.append(resolved[i])

                i += 1

            if forward_names:
                out.append(s.ScopedLetRec(forward_names, forward_values))

        return out

    def dump(self):
        parts = ['%s = %s' % (name.name, self.values[i].dump()) for (i, name) in enumerate(self.names)]
        return '(letrec %s)' % '; '.join(parts)

class StrictLet(LetGroup):
    def __init__(self, nodes):
        self.nodes = nodes

    def compile(self, tracker):
        return [tracker.visit(n) for n in self.nodes]

    def dump(self):
        return '(strict %s)' % '; '.join(n.dump() for n in self.nodes)

class LetGrouper(object):
    def __init__(self, clauses):
        self.clauses = clauses
        self.groups = []
        self.bound_names = []
        self.index = 0
        self.current = clauses[0]

    def next(self):
        self.index += 1
        if self.index < len(self.clauses):
            self.current = self.clauses[self.index]
        else:
            self.current = None

    def collect(self):
        while self.current:
            self.process_fresh()

        return self.groups

    def is_closeable(self):
        if not isinstance(self.current, s.Let): return False
        if not isinstance(self.current.body, s.Closeable): return False
        return True

    def process_fresh(self):
        if self.process_recursive(): return True
        if self.process_strict(): return True
        return False

    def process_recursive(self):
        recursive_names = []
        recursive_values = []
        while self.current:
            if not self.is_closeable(): break
            if self.current.bind in recursive_names: break
            recursive_names.append(self.current.bind)
            recursive_values.append(self.current.body)
            self.next()

        if recursive_names:
            self.groups.append(LetRec(recursive_names, recursive_values))
            return True
        else:
            return False

    def process_strict(self):
        strict_nodes = []
        while self.current:
            if self.is_closeable(): break
            strict_nodes.append(self.current)
            self.next()

        if strict_nodes:
            self.groups.append(StrictLet(strict_nodes))
            return True
        else:
            return False


class ScopeTracker(s.Visitor):
    def __init__(self, *name_providers):
        self.scope = []
        self.marks = []
        self.errors = []
        self.name_providers = name_providers
        self.unused_shadowed_stack = []
        self.binding_tracker = BindingTracker([], None)

    def resolve(self, semantic):
        out = self.visit(semantic)
        if self.errors: raise ScopeError(self.errors)
        if debug_scope: print 'scoped: ', out.dump()
        return out

    def error(self, message):
        self.errors.append(message)

    def lookup_namespace(self, name):
        # TODO
        return [sym(u'TODO')]

    def visit_lambda(self, lam):
        self.binding_tracker = BindingTracker(lam.binders, self.binding_tracker)
        resolved = self.visit_default(lam)

        closed = self.binding_tracker.closure
        locals = self.binding_tracker.all_locals

        self.binding_tracker = self.binding_tracker.parent

        return s.ClosedLambda(closed, locals, lam.binders, resolved.clauses)

    def visit_block(self, block):
        self.binding_tracker.start_block()

        groups = LetGrouper(block.nodes).collect()
        out_nodes = []

        for group in groups:
            out_nodes.extend(group.compile(self))

        self.binding_tracker.end_block()

        return s.Block(out_nodes)

    def visit_match_clause(self, clause):
        self.binding_tracker.start_block()
        resolved = self.visit_default(clause)
        self.binding_tracker.end_block()

        return resolved

    def visit_match_assign(self, assign):
        out = self.visit_default(assign)
        out.index = self.binding_tracker.bind(assign.symbol)
        return out

    def visit_name(self, name):
        try:
            return self.binding_tracker.lookup(name.symbol)
        except KeyError:
            i = 0
            length = len(self.name_providers)
            while i < length:
                try:
                    return self.name_providers[i].lookup(name.symbol)
                except KeyError:
                    i += 1

            self.error(u'unbound name %s' % name.symbol.name)

    def visit_let(self, let):
        # if previously bound, don't recurse
        new_body = self.visit(let.body)
        index = self.binding_tracker.bind(let.bind)

        return s.ScopedLet(let.bind, index, new_body)
