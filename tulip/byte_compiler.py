import tulip.semantic as s
import tulip.value as v
import tulip.byte as b
from tulip.byte import Instr
from tulip.runtime import Frame
from tulip.builtin import lookup_builtin

from tulip.debug import debug
debug_byte = debug.check('byte') or debug.check('runtime')

class CodeBuilder(object):
    def __init__(self):
        assert False, 'abstract'

    def register_local(self, name, index):
        pass

    def dump_instr(self, instr):
        (t, a1, a2) = instr
        return "    %s %s %s" % (Instr.INSTRS[t], self.dump_digit(a1), self.dump_digit(a2))

    def dump_digit(self, digit):
        if digit < 0:
            return '-'
        else:
            return str(digit)

class RootBuilder(CodeBuilder):
    def __init__(self):
        self.code = []
        self.local_names = {}
        self.max_local_index = -1

    def emit(self, instr, arg1, arg2):
        self.code.append((instr, arg1, arg2))

    def register_local(self, symbol, index):
        self.local_names[index] = symbol
        if index > self.max_local_index:
            self.max_local_index = index

    def dump(self):
        out = ''
        out += "root:\n"
        for code in self.code:
            out += self.dump_instr(code) + "\n"
        return out

class LambdaBuilder(CodeBuilder):
    def __init__(self, index, arg_count, closed_count, local_count):
        self.index = index
        self.arg_count = arg_count
        self.closed_count = closed_count
        self.local_count = local_count
        self.clauses = []

    def start_clause(self):
        self.clauses.append([])

    def emit(self, instr, arg1, arg2):
        self.clauses[-1].append((instr, arg1, arg2))

    def as_code(self):
        next_index = 0
        instrs = []
        for clause in self.clauses:
            next_index += len(clause)
            for instruction in clause:
                if instruction[0] == Instr.JUMP and instruction[1] == -1:
                    instrs.append((Instr.JUMP, next_index, -1))
                else:
                    instrs.append(instruction)

        return b.Lambda(instrs, self.index, self.arg_count, self.closed_count, self.local_count)

    def dump(self):
        out = ''
        out += "lambda#%d(%d, %d, %d):\n" % (self.index, self.arg_count, self.closed_count, self.local_count)
        for (j, clause) in enumerate(self.clauses):
            out += "  clause#%d:\n" % j
            for instr in clause:
                out += self.dump_instr(instr) + "\n"

        return out


class ByteCompiler(s.Visitor):
    def __init__(self):
        self.constants = []
        self.symbols = []

        # TODO: non-lambda-scoped locals
        self.root = RootBuilder()
        self.lambdas = []
        self.current_builder = self.root

    def compile(self, ast):
        self.visit(ast)
        code = self.as_code()
        if debug_byte: print 'bytecode:\n', code.dump()
        return code

    def as_code(self):
        code = b.RootCode(self.root.code, self.root.local_names, self.symbols, self.constants, self.lambdas, 'root')
        for l in self.lambdas:
            l.symbols = code.symbols
            l.constants = code.constants
            l.lambdas = code.lambdas

        return code

    def dump(self):
        out = ''
        out += "[constants]\n"
        for (i, const) in enumerate(self.constants):
            out += "#%d: %s\n" % (i, const.dump())
        out += "[symbols]"
        for (i, sym) in enumerate(self.symbols):
            out += "#%d: \"%s\"\n" % (i, sym)
        out += "[root]\n"
        out += self.root.dump()
        out += "[lambdas]\n"
        for (i, l) in enumerate(self.lambdas):
            out += l.dump()

        return out


    def sym(self, value):
        for (i, c) in enumerate(self.symbols):
            if c == value:
                return i

        self.symbols.append(value)
        return len(self.symbols) - 1

    def emit(self, instr, arg1=-1, arg2=-1):
        self.current_builder.emit(instr, arg1, arg2)

    def visit_scoped_let(self, let):
        self.visit_default(let)
        self.current_builder.register_local(let.bind, let.index)
        self.emit(Instr.STORL, let.index)

    def visit_local_name(self, local_name):
        self.current_builder.register_local(local_name.symbol, local_name.index)
        self.emit(Instr.LOADL, local_name.index)

    def visit_argument_name(self, argument_name):
        self.emit(Instr.LOADA, argument_name.index)

    def visit_closure_name(self, closure_name):
        self.emit(Instr.LOADC, closure_name.index)

    def visit_lambda(self, _):
        assert False, 'cannot compile unresolved lambda'

    def visit_closed_lambda(self, clam):
        for c in clam.closed:
            self.visit(c)

        builder = LambdaBuilder(len(self.lambdas), len(clam.binders), len(clam.closed), len(clam.locals))

        self.current_builder, old_builder = builder, self.current_builder

        for clause in clam.clauses:
            builder.start_clause()
            self.visit(clause)
            self.emit(Instr.RETRN)

        self.current_builder = old_builder

        self.lambdas.append(builder.as_code())

        self.emit(Instr.LAMBD, len(self.lambdas) - 1, len(clam.closed))

    def visit_block(self, block):
        size = len(block.nodes)
        for (i, n) in enumerate(block.nodes):
            self.visit(n)

            # pop off the value if it's a plain expression and not
            # the last expression in the block
            if isinstance(n, s.ScopedLetRec): continue
            if isinstance(n, s.ScopedLet): continue
            if i == size - 1: continue
            self.emit(Instr.POP, 1)

        # if the block ends with an assignment, load
        # the assigned value as the result of the block.
        last = block.nodes[-1]
        if isinstance(last, s.ScopedLet):
            self.emit(Instr.LOADL, last.index)
        elif isinstance(last, s.ScopedLetRec):
            (_, index) = last.names[-1]
            self.emit(Instr.LOADL, index)

    def visit_forward_name(self, forward_name):
        # emit a dummy value, will be patched with PATCH later,
        # see visit_scoped_letrec
        self.emit(Instr.BANG)

    def visit_scoped_letrec(self, letrec):
        size = len(letrec.names)
        to_patch = {}

        for i in xrange(0, size):
            value = letrec.values[i]
            (name, index) = letrec.names[i]

            for (findex, c) in enumerate(value.closed):
                if isinstance(c, s.ForwardName):
                    try:
                        to_patch[c.index].append((findex, index))
                    except KeyError:
                        to_patch[c.index] = [(findex, index)]

            self.visit(value)
            self.current_builder.register_local(name, i)
            self.emit(Instr.STORL, i)

            try:
                for (findex, vindex) in to_patch[index]:
                    self.emit(Instr.LOADL, i)
                    self.emit(Instr.LOADL, vindex)
                    self.emit(Instr.PATCH, findex)
            except KeyError:
                pass

    def visit_tag(self, tag):
        self.visit_default(tag)
        self.emit(Instr.MKTAG, len(tag.values), self.sym(tag.symbol.name))

    def visit_match_assign(self, match_assign):
        self.visit(match_assign.value)
        self.emit(Instr.STORL, match_assign.index)

    def visit_constant(self, constant):
        self.constants.append(constant.value)
        self.emit(Instr.CONST, len(self.constants) - 1)

    def visit_builtin(self, builtin):
        name = builtin.name.name
        try:
            impl = lookup_builtin(name)
            self.constants.append(v.Builtin(name, impl))
            self.emit(Instr.CONST, len(self.constants) - 1)
        except KeyError:
            self.error('missing builtin %s' % name)

    def visit_apply(self, apply_node):
        for i in xrange(1, len(apply_node.nodes)):
            self.visit(apply_node.nodes[i])

        self.visit(apply_node.nodes[0])

        self.emit(Instr.APPLY, len(apply_node.nodes) - 1)

    def visit_match_guard_tag(self, guard):
        self.visit(guard.value)
        self.emit(Instr.TESTT, self.sym(guard.tag.name), guard.arity)
        self.emit(Instr.JUMP, -1)

    def visit_tag_destruct(self, destruct):
        destruct.visit_children(self)
        self.emit(Instr.DETAG, destruct.index)

